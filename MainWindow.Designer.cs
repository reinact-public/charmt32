﻿
namespace charmt32
{
    partial class MainWindow
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label horizontalLine1;
            System.Windows.Forms.Panel navigatorPanel;
            System.Windows.Forms.FlowLayoutPanel navigatorButtonsPanel;
            System.Windows.Forms.Button saveButton;
            System.Windows.Forms.Button resetButton;
            System.Windows.Forms.TableLayoutPanel mainContentsPanel;
            System.Windows.Forms.Label verticalLine1;
            System.Windows.Forms.Panel WizardContentsPanel;
            System.Windows.Forms.Label titleLabel;
            this.imageMatrix = new charmt32.ImageMatrix();
            this.replacingModeRadioButton = new System.Windows.Forms.RadioButton();
            this.composingModeRadioButton = new System.Windows.Forms.RadioButton();
            this.saveFileDialog = new System.Windows.Forms.SaveFileDialog();
            horizontalLine1 = new System.Windows.Forms.Label();
            navigatorPanel = new System.Windows.Forms.Panel();
            navigatorButtonsPanel = new System.Windows.Forms.FlowLayoutPanel();
            saveButton = new System.Windows.Forms.Button();
            resetButton = new System.Windows.Forms.Button();
            mainContentsPanel = new System.Windows.Forms.TableLayoutPanel();
            verticalLine1 = new System.Windows.Forms.Label();
            WizardContentsPanel = new System.Windows.Forms.Panel();
            titleLabel = new System.Windows.Forms.Label();
            navigatorPanel.SuspendLayout();
            navigatorButtonsPanel.SuspendLayout();
            mainContentsPanel.SuspendLayout();
            WizardContentsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // horizontalLine1
            // 
            horizontalLine1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            horizontalLine1.Dock = System.Windows.Forms.DockStyle.Top;
            horizontalLine1.Location = new System.Drawing.Point(0, 0);
            horizontalLine1.Name = "horizontalLine1";
            horizontalLine1.Size = new System.Drawing.Size(526, 2);
            horizontalLine1.TabIndex = 0;
            // 
            // navigatorPanel
            // 
            navigatorPanel.Controls.Add(navigatorButtonsPanel);
            navigatorPanel.Controls.Add(horizontalLine1);
            navigatorPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            navigatorPanel.Location = new System.Drawing.Point(0, 309);
            navigatorPanel.Name = "navigatorPanel";
            navigatorPanel.Size = new System.Drawing.Size(526, 32);
            navigatorPanel.TabIndex = 1;
            // 
            // navigatorButtonsPanel
            // 
            navigatorButtonsPanel.Controls.Add(saveButton);
            navigatorButtonsPanel.Controls.Add(resetButton);
            navigatorButtonsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            navigatorButtonsPanel.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            navigatorButtonsPanel.Location = new System.Drawing.Point(0, 2);
            navigatorButtonsPanel.Name = "navigatorButtonsPanel";
            navigatorButtonsPanel.Size = new System.Drawing.Size(526, 30);
            navigatorButtonsPanel.TabIndex = 1;
            // 
            // saveButton
            // 
            saveButton.Location = new System.Drawing.Point(448, 3);
            saveButton.Margin = new System.Windows.Forms.Padding(12, 3, 3, 3);
            saveButton.Name = "saveButton";
            saveButton.Size = new System.Drawing.Size(75, 23);
            saveButton.TabIndex = 0;
            saveButton.Text = "&Save";
            saveButton.UseVisualStyleBackColor = true;
            saveButton.Click += new System.EventHandler(this.saveButton_Click);
            // 
            // imageMatrix
            // 
            this.imageMatrix.Location = new System.Drawing.Point(3, 3);
            this.imageMatrix.Name = "imageMatrix";
            this.imageMatrix.Size = new System.Drawing.Size(96, 128);
            this.imageMatrix.TabIndex = 0;
            this.imageMatrix.Text = "imageMatrix1";
            // 
            // resetButton
            // 
            resetButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resetButton.Location = new System.Drawing.Point(358, 3);
            resetButton.Name = "resetButton";
            resetButton.Size = new System.Drawing.Size(75, 23);
            resetButton.TabIndex = 1;
            resetButton.Text = "&Reset";
            resetButton.UseVisualStyleBackColor = true;
            resetButton.Click += new System.EventHandler(this.resetButton_Click);
            // 
            // mainContentsPanel
            // 
            mainContentsPanel.ColumnCount = 3;
            mainContentsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            mainContentsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 2F));
            mainContentsPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            mainContentsPanel.Controls.Add(this.imageMatrix, 0, 0);
            mainContentsPanel.Controls.Add(verticalLine1, 1, 0);
            mainContentsPanel.Controls.Add(WizardContentsPanel, 2, 0);
            mainContentsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            mainContentsPanel.Location = new System.Drawing.Point(0, 0);
            mainContentsPanel.Name = "mainContentsPanel";
            mainContentsPanel.RowCount = 1;
            mainContentsPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            mainContentsPanel.Size = new System.Drawing.Size(526, 309);
            mainContentsPanel.TabIndex = 2;
            // 
            // verticalLine1
            // 
            verticalLine1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            verticalLine1.Dock = System.Windows.Forms.DockStyle.Fill;
            verticalLine1.Location = new System.Drawing.Point(105, 0);
            verticalLine1.Name = "verticalLine1";
            verticalLine1.Size = new System.Drawing.Size(1, 309);
            verticalLine1.TabIndex = 1;
            // 
            // WizardContentsPanel
            // 
            WizardContentsPanel.Controls.Add(this.composingModeRadioButton);
            WizardContentsPanel.Controls.Add(this.replacingModeRadioButton);
            WizardContentsPanel.Controls.Add(titleLabel);
            WizardContentsPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            WizardContentsPanel.Location = new System.Drawing.Point(107, 3);
            WizardContentsPanel.Name = "WizardContentsPanel";
            WizardContentsPanel.Size = new System.Drawing.Size(416, 303);
            WizardContentsPanel.TabIndex = 2;
            // 
            // titleLabel
            // 
            titleLabel.AutoSize = true;
            titleLabel.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            titleLabel.Location = new System.Drawing.Point(5, 6);
            titleLabel.Name = "titleLabel";
            titleLabel.Size = new System.Drawing.Size(174, 24);
            titleLabel.TabIndex = 0;
            titleLabel.Text = "CHARMT32.EXE";
            // 
            // replacingModeRadioButton
            // 
            this.replacingModeRadioButton.AutoSize = true;
            this.replacingModeRadioButton.Checked = true;
            this.replacingModeRadioButton.Location = new System.Drawing.Point(9, 56);
            this.replacingModeRadioButton.Name = "replacingModeRadioButton";
            this.replacingModeRadioButton.Size = new System.Drawing.Size(104, 16);
            this.replacingModeRadioButton.TabIndex = 1;
            this.replacingModeRadioButton.TabStop = true;
            this.replacingModeRadioButton.Text = "Replacing mode";
            this.replacingModeRadioButton.UseVisualStyleBackColor = true;
            this.replacingModeRadioButton.CheckedChanged += new System.EventHandler(this.replacingModeRadioButton_CheckedChanged);
            // 
            // composingModeRadioButton
            // 
            this.composingModeRadioButton.AutoSize = true;
            this.composingModeRadioButton.Location = new System.Drawing.Point(9, 78);
            this.composingModeRadioButton.Name = "composingModeRadioButton";
            this.composingModeRadioButton.Size = new System.Drawing.Size(110, 16);
            this.composingModeRadioButton.TabIndex = 2;
            this.composingModeRadioButton.Text = "Composing mode";
            this.composingModeRadioButton.UseVisualStyleBackColor = true;
            this.composingModeRadioButton.CheckedChanged += new System.EventHandler(this.replacingModeRadioButton_CheckedChanged);
            // 
            // saveFileDialog
            // 
            this.saveFileDialog.DefaultExt = "png";
            this.saveFileDialog.Filter = "PNG Image|*.png";
            // 
            // MainWindow
            // 
            this.AcceptButton = saveButton;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = resetButton;
            this.ClientSize = new System.Drawing.Size(526, 341);
            this.Controls.Add(mainContentsPanel);
            this.Controls.Add(navigatorPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.Name = "MainWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CHARMT32.EXE";
            navigatorPanel.ResumeLayout(false);
            navigatorButtonsPanel.ResumeLayout(false);
            mainContentsPanel.ResumeLayout(false);
            WizardContentsPanel.ResumeLayout(false);
            WizardContentsPanel.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private ImageMatrix imageMatrix;
        private System.Windows.Forms.RadioButton composingModeRadioButton;
        private System.Windows.Forms.RadioButton replacingModeRadioButton;
        private System.Windows.Forms.SaveFileDialog saveFileDialog;
    }
}

