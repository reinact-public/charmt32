﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace charmt32
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void replacingModeRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            this.imageMatrix.ReplacingMode = replacingModeRadioButton.Checked;
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                using (Image image = this.imageMatrix.Image)
                {
                    image.Save(saveFileDialog.FileName);
                }
            }
        }

        private void resetButton_Click(object sender, EventArgs e)
        {
            this.imageMatrix.Clear();
        }

    }
}
