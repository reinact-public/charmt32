﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace charmt32
{
    public class ImageMatrix : Control
    {
        /// <summary>
        /// コントロールが保持する画像の幅を取得または設定します。
        /// </summary>
        public int ItemWidth { get; set; }

        /// <summary>
        /// コントロールが保持する画像の高さを取得または設定します。
        /// </summary>
        public int ItemHeight { get; set; }

        /// <summary>
        /// コントロールが保持する画像表の行数を取得または設定します。
        /// </summary>
        public int Rows { get; set; }

        /// <summary>
        /// コントロールが保持する画像表の列数を取得または設定します。
        /// </summary>
        public int Columns { get; set; }

        /// <summary>
        /// 画像を置き換えるかどうかを示す値を取得または設定します。
        /// </summary>
        public bool ReplacingMode { get; set; }

        /// <summary>
        /// コントロールが表す画像を取得します。
        /// </summary>
        public Image Image
        {
            get
            {
                Bitmap bitmap = new Bitmap(this.ItemWidth * this.Columns, this.ItemHeight * this.Rows);
                using (Graphics g = Graphics.FromImage(bitmap))
                {
                    for (int i = 0; i < this.Rows; ++i)
                    {
                        for (int j = 0; j < this.Columns; ++j)
                        {
                            if (this.imageMatrix[i, j] != null)
                            {
                                g.DrawImage(this.imageMatrix[i, j], j * this.ItemWidth, i * this.ItemHeight);
                            }
                        }
                    }
                }
                return bitmap;
            }
        }

        private Image[,] imageMatrix;


        public ImageMatrix()
        {
            this.ItemWidth = 32;
            this.ItemHeight = 32;
            this.Rows = 4;
            this.Columns = 3;
            this.ReplacingMode = true;
            this.Clear();

            this.AllowDrop = true;
            this.DragEnter += ImageMatrix_DragEnter;
            this.DragDrop += ImageMatrix_DragDrop;
        }

        /// <summary>
        /// コントロールが保持する画像を消去します。
        /// </summary>
        public void Clear()
        {
            this.imageMatrix = new Image[this.Rows, this.Columns];
            this.Invalidate();
        }

        protected override void OnPaint(PaintEventArgs pe)
        {
            base.OnPaint(pe);
            using (Graphics g = pe.Graphics)
            {
                int tileWidth = this.ItemWidth / 2;
                int tileHeight = this.ItemHeight / 2;

                for (int i = 0; i * tileWidth < this.Width; ++i)
                {
                    for (int j = 0; j * tileHeight < this.Height; ++j)
                    {
                        g.FillRectangle((i + j) % 2 == 0 ? Brushes.Blue : Brushes.DarkBlue,
                            i * tileWidth, j * tileHeight, tileWidth, tileHeight);
                    }
                }
                using (Image image = this.Image)
                {
                    g.DrawImage(image, 0, 0);
                }
            }
        }

        private void ImageMatrix_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Copy;
            }
        }

        private void ImageMatrix_DragDrop(object sender, DragEventArgs e)
        {
            Point loc = this.PointToClient(new Point(e.X, e.Y));
            int row = loc.Y / this.ItemHeight;
            int col = loc.X / this.ItemWidth;
            if (row < 0 || row >= this.Rows || col < 0 || col >= this.Columns)
            {
                return;
            }

            string fileName = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            try
            {
                if (this.imageMatrix[row, col] == null || this.ReplacingMode)
                {
                    this.imageMatrix[row, col] = Image.FromFile(fileName);
                }
                else
                {
                    using (Graphics g = Graphics.FromImage(this.imageMatrix[row, col]))
                    {
                        g.DrawImage(Image.FromFile(fileName), 0, 0);
                    }
                }
            }
            catch
            {
                MessageBox.Show(string.Format(Properties.Resources.EMSG_UNSUPPORTED_FILE, fileName),
                    "CHARMT32.EXE", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            this.Invalidate();
        }
    }
}
